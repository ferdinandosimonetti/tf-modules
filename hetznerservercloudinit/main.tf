resource "hcloud_server" "projectserver" {
  name = var.name
  image = var.image
  server_type = var.server_type
  user_data = file(var.userdatafile)
  ssh_keys = [ var.sshkeysname ]
  labels = var.labels
}