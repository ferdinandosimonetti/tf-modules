output "id" {
  value       = "${hcloud_server.projectserver.id}"
  description = "ID of server"
}
output "name" {
  value       = "${hcloud_server.projectserver.name}"
  description = "server name"
}
output "address" {
  value       = "${hcloud_server.projectserver.ipv4_address}"
  description = "server name"
}