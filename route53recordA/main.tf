# extract zone ID from zone name
data "aws_route53_zone" "selected" {
  name         = var.zonename
}

resource "aws_route53_record" "dnsrecord" {
  zone_id = data.aws_route53_zone.selected.zone_id
  name    = "${var.src}.${var.zonename}"
  type    = "A"
  ttl     = "300"
  records = var.dst
}