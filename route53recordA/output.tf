output "fqdn" {
  value       = "${aws_route53_record.dnsrecord.fqdn}"
  description = "FQDN of DNS record"
}