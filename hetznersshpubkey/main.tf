resource "hcloud_ssh_key" "projectkey" {
  name = var.keyname
  public_key = file(var.keyfile)
}