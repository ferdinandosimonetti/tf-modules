output "id" {
  value       = "${hcloud_ssh_key.projectkey.id}"
  description = "SSH public key ID"
}
output "name" {
  value        = "${hcloud_ssh_key.projectkey.name}"
  description = "SSH public key name"
}