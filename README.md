# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This is a collection of custom Terraform modules to interact with public cloud providers
* Version 0.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* **Usage instructions**: these modules are meant to be referenced with the specific Bitbucket+subdirs syntax

`module "mymodule" {
    source = "bitbucket.org/ferdinandosimonetti/tf-modules//mymodule"
}`

### Who do I talk to? ###

* **Repo owner or admin**: info@fsimonetti.eu