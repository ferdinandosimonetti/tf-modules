variable "name" {}
variable "defaultserver_type" {}
variable "defaultimage" {}
variable "userdatafile" {}
variable "sshkeysnames" {
    type = list(string)
}
variable "labels" {}