resource "hcloud_server" "projectserver" {
  name = var.name
  image = var.defaultimage
  server_type = var.defaultserver_type
  user_data = file(var.userdatafile)
  ssh_keys = var.sshkeysnames
  labels = var.labels
}